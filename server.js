const { response } = require('express')
const express = require('express')
const app = express()

app.use(express.json())
app.get('/',(request,response)=>{
    response.send("Inside A container");
})

app.listen(3000,"0.0.0.0",()=>{
    console.log("Server Started");
})